<?xml version="1.0" encoding="UTF-8"?>
<aiml version="2.0">
<!-- File: Nivel0.aiml -->
<!-- Author: Alejandro Villanueva Prados-->
<!-- Last modified:  -->
<!--  -->
<!--  
Archivo de la Práctica 1, nivel 0 de la asignatura Inteligencia Articial del
Doble Grado de Ingeniería Informáctica de la Universidad de Granada. Curso
2020-2021 
-->
<!--  -->

<!-- Regla genérica para indicar estación actual -->
<category>
	<pattern>
		_ESTACION
	</pattern>
	<template>
		Estamos en <srai>SEASON</srai>
	</template>
</category>

<!-- Regla genérica para indicar fase del día -->
<category>
	<pattern>
		_FASEDIA
	</pattern>
	<template>
		Ahora estamos en la <srai>DAY PHASE</srai>
	</template>
</category>

<!-- Regla general para responder con el día de hoy -->
<category>
	<pattern>
		_HOY
	</pattern>
	<template>
		Hoy es <srai>DAY</srai>
	</template>
</category>

<!-- Regla general para responder con el día de mañana -->
<category>
	<pattern>
		_MANYANA
	</pattern>
	<template>
		Manyana es <srai>TOMORROW</srai>
	</template>
</category>

<!-- Regla general para responder con el día de pasado (nañana) -->
<category>
	<pattern>
		_PASADO
	</pattern>
	<template>
		Pasado manyana es <srai>DAY AFTER TOMORROW</srai>
	</template>
</category>

<!-- Regla que hace de switch para determinar qué día nos piden -->
<category>
	<pattern>
		_CALCULA_DIA *
	</pattern>
	<template>
		<think><set var="input"><star/></set></think>
		<condition var="input">
			<li value="hoy"><srai>_HOY</srai></li>
			<li value="manyana"><srai>_MANYANA</srai></li>
			<li value="pasado"><srai>_PASADO</srai></li>
			<li> Lo siento, no entiendo qué me estás pidiendo.</li>
		</condition>
	</template>
</category>

<!-- Regla genérica para responder el día dentro de una semana -->
<category>
	<pattern>
		_SIGUIENTE_SEMANA
	</pattern>
	<template>
		Dentro de una semana será <srai>DATE IN 7 DAYS</srai>
	</template>
</category>

<!-- Regla genérica para responder el día que del próximo lunes/martes etc -->
<category>
	<pattern>
		_PROX_DIA <set>weekday_es</set>
	</pattern>
	<template>
		El próximo <star/> será <srai>DATE A WEEK FROM <star/></srai>
	</template>
</category>

<!-- Pregunta sobre estacion del año -->
<category>
	<pattern>
		(en) [cual que] ^ [estacion epoca] (del anyo) [estamos es actual]
	</pattern>
	<template>
		<srai>_ESTACION</srai>
	</template>
</category>

<!-- Pregunta sobre la fase del día -->
<category>
	<pattern>
		(en) [que cual] ^ fase del dia ^ (estamos es)
	</pattern>
	<template>
		<srai>_FASEDIA</srai>
	</template>
</category>

<!-- Pregunta para que dia de la semana es hoy/manyana/pasado -->
<category>
	<pattern>
		(en) que dia de la semana [es cae] *  (manyana)
	</pattern>
	<template>
		<srai>_CALCULA_DIA <star/></srai>
	</template>
</category>

<!-- Regla para que fecha sera en una semana -->
<category>
	<pattern>
		que [dia fecha] [sera tendremos] [dentro en] ^ una semana
	</pattern>
	<template>
		<srai>_SIGUIENTE_SEMANA</srai>
	</template>
</category>

<!-- Para la pregunta que fecha sera el proximo DIA_SEMANA -->
<category>
	<pattern>
		que [fecha dia] sera ^ [proximo siguiente] <set>weekday_es</set>
	</pattern>
	<template>
		<srai>_PROX_DIA <star index="2"/></srai>
	</template>
</category>


</aiml>
